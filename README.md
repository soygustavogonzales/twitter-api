**#First step: clone this repository.**

$ git clone https://bitbucket.org/soygustavogonzales/twitter-api

**#Second step: install dependencies.**

$ npm install

**#Last step.**

$ npm start

**#Finally: open your browser: Chrome is better.**

localhost:3000

![pantalla-twitter-app.jpg](https://bitbucket.org/repo/7y8Kej/images/702023999-pantalla-twitter-app.jpg)